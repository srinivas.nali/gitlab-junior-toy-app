module QA
  module Resource
    class Group < Base
      attributes :name,
                 :description,
                 :owner_id

      attribute :id do
        raise NoValueError unless current_url.include?('group')
        URI(current_url).path.split('/').last.to_i
      end

      def initialize
        @fabrication_success_text = 'Group created!'
      end

      def fabricate_via_browser_ui!
        QA::Page::Group::New.perform do |new|
          new.fill_name_field(name)
          new.fill_description_field(description)
          new.click_submit_form_button
        end
      end

      def perform_post_ui_fabrication_hook
        populate(:id)
      end

      def api_delete_path
        "/groups/#{id}"
      end

      def name
        @name ||= FFaker::FreedomIpsum.word
      end

      def description
        @description ||= FFaker::FreedomIpsum.phrase
      end
    end
  end
end
