module QA
  module Page
    module Group
      class New < Base
        def fill_name_field(email)
          fill_element(:name_field, email)
        end

        def fill_description_field(password)
          fill_element(:description_field, password)
        end

        def click_submit_form_button
          click_element(:submit_form_button)
        end
      end
    end
  end
end
