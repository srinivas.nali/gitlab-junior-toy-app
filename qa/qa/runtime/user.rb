module QA
  module Runtime
    module User
      extend self

      def admin
        QA::Resource::User.init do |user|
          user.email = admin_email
          user.password = admin_password
        end
      end

      def default
        QA::Resource::User.init do |user|
          user.email = default_email
          user.password = default_password
        end
      end

      def default_email
        'admin@gitlabjunior.com'
      end

      def default_password
        '5iveL!fe'
      end

      def admin_email
        Runtime::Env.admin_email || default_email
      end

      def admin_password
        Runtime::Env.admin_password || default_password
      end
    end
  end
end
