require_relative '../../spec_helper'

RSpec.describe 'Personal Access Token' do
  before do
    QA::Flow::Login.sign_in
  end

  it 'can be created via the UI' do
    personal_access_token = QA::Resource::PersonalAccessToken.fabricate!

    expect(personal_access_token).not_to be_nil
  end
end
