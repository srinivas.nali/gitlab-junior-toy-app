module QA
  module Support
    module API
      class Request
        API_VERSION = 'v1'

        def initialize(api_client, path, **query_string)
          query_string[:private_token] ||= api_client.personal_access_token
          @request_path = get_request_path(path, **query_string)
        end

        def url
          QA::Runtime::Env.app_url + @request_path
        end

        # Prepend a request path with the path to the API
        #
        # path - Path to append
        #
        # Examples
        #
        #   >> request_path('/issues')
        #   => "/api/v4/issues"
        #
        #   >> request_path('/issues', private_token: 'sometoken)
        #   => "/api/v4/issues?private_token=..."
        #
        # Returns the relative path to the requested API resource
        def get_request_path(path, version: API_VERSION, **query_string)
          full_path = ::File.join('/api', version, path)

          if query_string.any?
            full_path << (path.include?('?') ? '&' : '?')
            full_path << query_string.map { |k, v| "#{k}=#{CGI.escape(v.to_s)}" }.join('&')
          end

          full_path
        end
      end
    end
  end
end
