FactoryBot.define do
  factory :user do
    name { FFaker::Name.name }
    email  { FFaker::Internet.email }
    password_digest { User.digest('password') }

    trait :admin do
      admin { true }
    end
  end
end
