FactoryBot.define do
  factory :group do
    name { FFaker::Lorem.word }
    description { FFaker::Lorem.paragraph }
    owner factory: :user

    trait :with_parent do
      parent_group factory: :group
    end
  end
end
