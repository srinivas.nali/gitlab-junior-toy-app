require 'rails_helper'

RSpec.describe PersonalAccessToken, type: :model do
  let(:personal_access_token) { create(:personal_access_token) }

  it 'is valid' do
    expect(personal_access_token).to be_valid
  end

  it 'needs a name' do
    personal_access_token.name = nil
    expect(personal_access_token).not_to be_valid
  end

  it 'needs a value' do
    personal_access_token.value = nil
    expect(personal_access_token).not_to be_valid
  end

  it 'should be associated with a user' do
    personal_access_token.user_id = nil
    expect(personal_access_token).not_to be_valid
  end

end
