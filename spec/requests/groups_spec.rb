require 'rails_helper'

RSpec.describe "Groups", type: :request do
  let(:user) { create(:user) }
  let(:admin_user) { create(:user, :admin) }
  let(:other_user) { create(:user) }
  let(:group) { create(:group, owner: user) }
  let(:invalid_post_params) do
    { group: { name: '', description: 'description'  } }
  end

  let(:valid_post_params) do
    { group: { name: 'group_name', description: 'description'  } }
  end

  describe 'POST /groups' do
    context 'when not logged in' do
      it 'redirects to index' do
        post groups_path, params: valid_post_params
        expect(response).to redirect_to(login_url)
      end
    end

    context 'when logged in' do
      before do
        log_in_as(user)
      end

      it 'does not save with invalid params' do
        expect { post groups_path, params: invalid_post_params }.to_not change { Group.count }
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'saves with valid params' do
        expect { post groups_path, params: valid_post_params }.to change { Group.count }
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(group_path(Group.last.id))
      end
    end
  end

  describe 'PATCH /group' do
    context 'when not logged in' do
      it 'redirects to index' do
        patch group_path(group.id), params: valid_post_params
        expect(response).to redirect_to(login_url)
      end
    end

    context 'when logged in as correct user' do
      before do
        log_in_as(user)
      end

      it 'does not save with invalid params' do
        patch group_path(group.id), params: invalid_post_params
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it 'saves with valid params' do
        patch group_path(group.id), params: valid_post_params
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(group_path(group.id))
      end
    end

    context 'when logged in as another user' do
      before do
        log_in_as(other_user)
      end

      it 'does not save' do
        patch group_path(group.id), params: valid_post_params
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe 'DELETE /group' do
    context 'when not logged in' do
      it 'redirects to index' do
        delete group_path(group.id)
        expect(response).to redirect_to(login_url)
      end
    end

    context 'when logged in as owner user' do
      before do
        log_in_as(user)
        group
      end

      it 'deletes the group' do
        expect { delete group_path(group.id), params: valid_post_params }.to change { Group.count }
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_url)
      end
    end

    context 'when logged in as admin user' do
      before do
        log_in_as(admin_user)
        group
      end

      it 'deletes the group' do
        expect { delete group_path(group.id), params: valid_post_params }.to change { Group.count }
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_url)
      end
    end

    context 'when logged as another user' do
      before do
        log_in_as(other_user)
        group
      end

      it 'deletes the group' do
        expect { delete group_path(group.id), params: valid_post_params }.not_to change { Group.count }
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end

def log_in_as(user, password: 'password', remember_me: '1')
  post login_path, params: { session: { email: user.email,
    password: password,
    remember_me: remember_me } }
end
