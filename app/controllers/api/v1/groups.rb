module API
  module V1
    class Groups < Grape::API
      include API::V1::Defaults

      resource :groups do
        desc 'Return all groups' do
          success Entities::Group
        end
        params do
          optional :owner_id, type: Integer, desc: 'ID of the owner'
        end
        get do
          groups = params[:owner_id].present? ? Group.where(owner_id: params[:owner_id]) : Group.all
          present groups, with: Entities::Group
        end

        desc 'Return a group'
        params do
          requires :id, type: Integer, desc: 'ID of the group'
        end
        get ':id' do
          group = Group.where(id: params[:id]).first!
          present group, with: API::Entities::Group
        end

        desc 'Delete a group'
        params do
          requires :id, type: Integer, desc: 'ID of the group'
        end
        delete ':id' do
          authenticate!

          group = Group.find(params[:id])

          unauthorized! unless current_user.admin? || current_user == group.owner

          Group.find(params[:id]).destroy
          status 204
        end

        desc 'Return a group\'s subgroups'
        params do
          requires :id, type: Integer, desc: 'ID of the group'
        end
        get ':id/subgroups' do
          group = Group.where(id: params[:id]).first!.sub_groups
          present group, with: API::Entities::Group
        end

        desc 'Create a group'
        params do
          requires :name, type: String, desc: 'The name of the new group'
          requires :description, type: String, desc: 'The description of the new group'
          optional :parent_group_id, type: Integer, desc: 'Id of the parent group'
        end
        post do
          authenticate!

          params = permitted_params
          group = Group.new(params.merge({owner_id: current_user.id}))
          if group.save
            present group, with: API::Entities::Group
          else
            render_validation_error!(group)
          end
        end
      end
    end
  end
end
